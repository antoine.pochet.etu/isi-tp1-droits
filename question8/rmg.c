#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <grp.h>
#include <sys/stat.h>
#include <sys/types.h>

void deleteFile(char * filename){
    int del = remove(filename);
    if (del == 0)
        printf("File bien supprimé\n");
    else
        printf("File pas suppriméd");
}

gid_t extractGroupFile(char * filename){
    struct stat stats;
    if(stat(filename, &stats) == -1){
        printf("Impossible de récupérer le groupe du fichier \n");
        exit(EXIT_FAILURE);
    }
    return stats.st_gid;
}

int check_gid(gid_t filenameId, gid_t userId){
    if(filenameId == userId){
        return 0;
    }else{
        return -1;
    }
}

int check_passwd(char *password, char *username){
    FILE *file = NULL;
    char user[255];
    char pswd[255];

    file = fopen("../../admin1/passwd", "r");
    if (file == NULL){
        exit(EXIT_FAILURE);
    }

    while(fgets(user, 255, file)){
        user[strcspn(user, "\n")] = 0;

        if(strcmp(user, username) == 0){

            fgets(pswd, 255, file);
            pswd[strcspn(pswd, "\n")] = 0;

            if(strcmp(pswd, password) == 0){
                return 0;
            }
        }
        printf("Mauvais mot de passe");

    fclose(file);
    exit(EXIT_FAILURE);

    }
}

int main(int argc, char **argv){
    
    gid_t gfile = extractGroupFile(argv[1]);
    gid_t guser = getgid();
    char *username = getenv("USER");
    char password[20];

    if(check_gid(gfile, guser) == 0){
        printf("Entrez votre mot de passe : ");
        fgets(password, 20, stdin);
        password[strcspn(password, "\n")] = 0;

        if(check_passwd(password, username)==0) deleteFile(argv[1]);
        else printf("Mot de pass incorrect.\n");
    }else{
        printf("Vous n'avez pas les droits de le supprimer\n");
    }

}