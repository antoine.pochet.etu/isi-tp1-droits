#!/bin/bash

FILE_TO_DELETE="dir_a/file.txt"
PASSWORD="lamda_a_password"

if TRY=$(printf "$PASSWORD\n" | ./rmg "$FILE_TO_DELETE"); then
  echo "script OK"
else
  echo "script KO : $?"
fi