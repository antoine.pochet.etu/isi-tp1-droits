# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Michot, Julien, julien.michot.etu@univ-lille.fr

- Pochet, Antoine, antoine.pochet.etu@univ-lille.fr

## Question 1

On regarde dans quel groupe est notre utilisateur *toto*. 
```
ubuntu@VM: groups toto
toto : ubuntu
```
*toto* est donc dans le group ubuntu. 
On affiche le contenu du dossier `ls -la`
```
ls -la
-r--rw-r-- 1 toto ubuntu 5 Jan 5 09:35 titi.txt
```
Non le processus ne pourra pas ouvrir en écriture le fichier car *toto* est le propriétaire du fichier et il n'a que la permission de lecture.

## Question 2

- Le caractère `x` signifie que le répertoire est éxecutable (il autorise l'accès (le traverser))
- Le dossier `mydir` n'est pas accessible pour *toto* car il n'a aucun droit d'accès sur le répertoire (le dossier n'est pas exécutable pour le groupe ubuntu et tous les autres utilisateur sauf ubuntu (user))
```
toto@VM: cd mydir
bash: cd: mydir/: Permission denied
```
- On obtient le resultat ci-dessous car il ne peut pas entrer dans le répertoire mais il a tout de même les droits de lecture.
```
toto@VM: ls -al mydir/
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt
```

## Question 3

- Lorsque l'on lance le programme avec l'utilisateur *toto* on obient les ids suivants :
```
euid = 1001
egid = 1000
ruid = 1001
rgid = 1000
```
Le processus n'arrive pas a ouvrir le fichier `mydir/data.txt` en lecture car l'utilisateur *toto*, qui éxecute le processus, n'a pas la permission.

- Les valeurs des ids sont maintenant :
```
euid = 1000
egid = 1000
ruid = 1001
rgid = 1000
```
Le processus arrive a ouvrir le fichier `mydir/data.txt` en lecture car le fichier est exécuté avec les droits du propriétaire, qui lui a les droits d'exécution.

## Question 4

Les valeurs des ids sont :
```
euid = 1001
egid = 1000
```

## Question 5

- La commande `chfn` permet de modifier les informations personnelles des utilisateurs contenues dans le fichier `/etc/passwd`.

Voici le résultat de `ls -al /usr/bin/chfn` :
```
ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```
On a donc root en utilisateur propriétaire et le groupe est root. On peut voir que le flag `set-user-id` est activé, cela permet à tous les utilisateurs d'accéder au fichier mais seul root pourra le modifier.

- Grâce à la commande `chfn` on a pu modifier les informations personnelles de toto et elles ont bien été modifiées dans le fichier `/etc/passwd`.

## Question 6

Les mots de passe des utilisateurs sont stockés dans /etc/shadow, ils ne sont accessibles uniquement par l'utilisateur *root* et ils ont subi un hash. Ces mots de passe sont inaccessibles des utilisateurs car ce sont des données sensibles qui donnent un accès intrégrale au contenu détenu par un utilisateur.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

Fichier *admin.sh* :

Grace a cette ligne il peut modifier des fichiers ou en créer de nouveaux dans dir_c
```
chmod u+rw dir_c
```
Grace a cette ligne il peut effacer ou renommer des fichiers dans dir_a, dir_b et dir_c
```
chmod u+rw dir_a dir_b
```

Fichier *groupe_a.sh* :

Grace a cette ligne ils peuvent lire tous les fichiers et sous-répertoires contenus dans dir_a et dir_c
```
chmod g+r dir_a dir_c
```
Grace a cette ligne ils peuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer, ni créer des nouveaux fichiers
```
chmod g-w dir_c
```
Grace a cette ligne ils peuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_a, et peuvent créer de nouveaux fichiers et répertoires dans dir_a 
```
chmod g+w dir_a
```
Grace a cette ligne ils n’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_a qui ne leur appartiennent pas
```
chmod +t dir_a
```
Grace a cette ligne ils ne peuvent ni lire, ni modifier, ni effacer les fichiers dans dir_b, et ne peuvent pas créer des nouveaux fichiers dans dir_b.
```
chmod o-rw dir_b
```

Fichier *groupe_a.sh* :

Grace a cette ligne ils peuvent lire tous les fichiers et sous-répertoires contenus dans dir_b et dir_c
```
chmod g+r dir_b dir_c
```
Grace a cette ligne ils peuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer, ni créer des nouveaux fichiers
```
chmod g-w dir_c
```
Grace a cette ligne ils peuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_b, et peuvent créer de nouveaux fichiers et répertoires dans dir_b 
```
chmod g+w dir_b
```
Grace a cette ligne ils n’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_b qui ne leur appartiennent pas 
```
chmod +t dir_b
```
Grace a cette ligne ils ne peuvent pas ni lire, ni modifier, ni effacer les fichiers dans dir_a, et ne peuvent pas créer des nouveaux fichiers dans dir_a
```
chmod o-rw dir_a
```

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








