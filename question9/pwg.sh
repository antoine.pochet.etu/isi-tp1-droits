#!/bin/bash

ARGS="$1\n"

if [ $# -eq 1 ]; then
  echo "Création d'un nouveau mot de passe"
else
  echo "Modification du mot de passe"
  ARGS="$ARGS$2\n"
fi

if TRY=$(printf $ARGS | ./pwg); then
  echo "script OK"
else
  echo "script KO : $?"
fi