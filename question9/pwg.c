#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <grp.h>
#include "check_pass.h"

char* encrypter(char *password) {
    unsigned long seed[2];
    char salt[] = "$1$ierfdjef";
    const char *const seedchars = 
    "./0123456789ABCDEFGHIJKLMNOPQRST"
    "UVWXYZabcdefghijklmnopqrstuvwxyz";
    int i;

    seed[0] = time(NULL);
    seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);

    for (i = 0; i < 8; i++)
        salt[3+i] = seedchars[(seed[i/5] >> (i%5)*6) & 0x3f];

    crypt(password, salt);
    return password;
}

void swapPwdFile(){
    //remove /etc/admini/passwd
    removeFile(PATH_PWD_FILE);
    //rename temp in /etc/admini/temp
    rename(PATH_TEMP_FILE,PATH_PWD_FILE);
}

int main(int argc, char *argv[])
{
    char * username = getenv("USER");
    char str[PASSWORD_MAX_SIZE];
    FILE* f = fopen(PATH_PWD_FILE, "r");
    FILE* temp = fopen(PATH_TEMP_FILE, "w");
    char *oldPwd = NULL;
    char str2[BUFFER_MAX_SIZE];
    char newPwd[PASSWORD_MAX_SIZE];
    int firstPwd = 1;

    if(f==NULL){
        printf("Impossible d'acceder au passwords");
        exit(1);
    }

    while (fgets(str, 20, f) != NULL )
    {
        fputs(str, temp);
        str[strcspn(str, "\n")] = 0;
        if(strcmp(username,str)==0){
            firstPwd = 0;
            fgets(str, 20, f);
            str[strcspn(str, "\n")] = 0;
            oldPwd = str;
            if(oldPwd!=NULL){
                //TODO : compare crypt pwd
                do
                {
                    printf("saisir votre ancien mot de passe: ");
                    fgets(str2, 20, stdin);
                    str2[strcspn(str2, "\n")] = 0;
                    if (strcmp(oldPwd, str2) != 0)
                        printf("mot de passe incorrect\n");
                } while (strcmp(oldPwd, str2) != 0);
            }

            insertNewPaswd(firstPwd, username, newPwd, temp);
        }
    }

    if(firstPwd==1)
        insertNewPaswd(firstPwd, username, newPwd, temp);
    
    swapPwdFile();
    fclose(f);

    return 0;
}
