#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int printIDValues()
{
    printf("euid = %d", geteuid());
    printf("\negid = %d", getegid());
    printf("\nruid = %d", getuid());
    printf("\nrgid = %d\n", getgid());

    return 0;
};

int openFile()
{
    FILE* file = fopen("../mydor/data.txt", "r");
    int currentChar = 0;

    if(file == NULL){
        perror("Can't open file");
        exit(EXIT_FAILURE);
    }else{
        while(currentChar != EOF){
            currentChar = fgetc(file);
            printf("%c", currentChar);
        }
        fclose(file);
    }
    return 0;
}

int main(int argc, char *argv[])
{
    printIDValues();
    openFile();
    return 0;
};